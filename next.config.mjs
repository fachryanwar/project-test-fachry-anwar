/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      "storage.googleapis.com",
      "assets.suitdev.com",
      "suitmedia.static-assets.id",
    ],
    unoptimized: true,
  },
};

export default nextConfig;
