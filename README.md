# Project Test Suitmedia

## Author:
Fachry Anwar Hamsyana, Universitas Indonesia  
Visit me at: [My Portfolio](https://fachryanwar.vercel.app/)

## Description
This project was created as a selection task for the Frontend Developer Intern position at Suitmedia MSIB Batch 7.

## How to run this project
1. Create a `.env` file from `.env.example`:
    ```bash
    cp .env.example .env
    ```
2. Run the Next.js project:
    ```bash
    npm install
    npm run dev
    ```

## Deployment link
[Visit Page](https://project-test-fachry-anwar.vercel.app/)
