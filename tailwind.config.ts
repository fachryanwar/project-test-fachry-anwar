import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        theme: {
          type: {
            primary: "#121312",
            secondary: "#636669",
            disabled: "#dddee0",
          },
          primary: {
            main: "#FF6600",
            secondary: "#FF8635",
            light: "#8fd0f7",
            dark: "#296a91",
            logo: "#0f75bc",
          },
          success: {
            main: "#4CC88e",
            background: "#C3ffe3",
          },
          error: {
            main: "#ff4949",
            background: "#ffc7c7",
          },
          warning: {
            main: "#ffc700",
            background: "#feffc7",
          },
          bg: {
            main: "#e6eef0",
          },
        },
        primary: {
          10: "#daeffc",
          20: "#b5dffa",
          30: "#8fd0f7",
          40: "#6ac0f5",
          50: "#45b0f2",
          60: "#378dc2",
          70: "#296a91",
          80: "#1c4661",
          90: "#0e2330",
        },
        danger: {
          10: "#facedd",
          20: "#f59ebb",
          30: "#ef6d98",
          40: "#ea3d76",
          50: "#e50c54",
          60: "#b70a43",
          70: "#890732",
          80: "#5c0522",
          90: "#2e0211",
        },
        warning: {
          10: "#fdf6cd",
          20: "#fbed9b",
          30: "#f9e36a",
          40: "#f7da38",
          50: "#f5d106",
          60: "#c4a705",
          70: "#937d04",
          80: "#625402",
          90: "#312a01",
        },
      },
      boxShadow: {
        20: "0px 0.5px 2px rgba(65, 78, 98, 0.12), 0px 0px 1px rgba(65, 78, 98, 0.05)",
        40: "0px 2px 4px rgba(65, 78, 98, 0.12), 0px 0px 1px rgba(65, 78, 98, 0.05)",
        60: "0px 4px 8px rgba(65, 78, 98, 0.12), 0px 0px 1px rgba(65, 78, 98, 0.05);",
        80: "0px 8px 16px rgba(65, 78, 98, 0.12), 0px 0px 1px rgba(65, 78, 98, 0.05);",
        100: "0px 16px 24px rgba(65, 78, 98, 0.12), 0px 0px 1px rgba(65, 78, 98, 0.05);",
      },
      backgroundImage: {
        "idea-background": "url('/images/idea-background.png')",
        "home-background": "url('/images/rocket-bg.jpg')",
      },
    },
  },
  plugins: [],
};
export default config;
