import clsxm from "@/lib/clsxm";
import { ReactNode } from "react";
import Typography from "./Typography";

enum ButtonVariant {
  "primary",
  "outline",
}

enum ButtonRounded {
  "full",
  "lg",
}

const Button = ({
  children,
  onClick,
  className,
  rounded = "full",
  variant = "primary",
}: {
  rounded?: keyof typeof ButtonRounded;
  variant?: keyof typeof ButtonVariant;
  children: ReactNode;
  className?: string;
  onClick?: () => void;
}) => {
  return (
    <button
      onClick={() => onclick}
      className={clsxm(
        " px-3 py-2 active:ring-1",
        [
          variant === "primary" &&
            "bg-theme-primary-secondary text-white hover:bg-theme-primary-main active:ring-gray-700",
        ],
        [
          variant === "outline" &&
            "border-2 border-white bg-transparent text-white hover:bg-white hover:text-slate-800 active:ring-gray-700",
        ],
        [rounded === "full" && "rounded-full"],
        [rounded === "lg" && "rounded-lg"],
        className
      )}
    >
      <Typography variant="p2">{children}</Typography>
    </button>
  );
};

export default Button;
