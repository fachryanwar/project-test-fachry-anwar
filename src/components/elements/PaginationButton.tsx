import clsxm from "@/lib/clsxm";
import Typography from "./Typography";

const PaginationButton = ({
  page,
  pageNumber,
  setPageNumber,
}: {
  page: number;
  pageNumber: number;
  setPageNumber: (page: number) => void;
}) => {
  return (
    <button
      className={clsxm(
        "rounded-lg p-1",
        pageNumber == page
          ? "bg-theme-primary-secondary text-white"
          : "hover:bg-gray-200"
      )}
      onClick={() => setPageNumber(page)}
    >
      <Typography variant="p2">{page}</Typography>
    </button>
  );
};

export default PaginationButton;
