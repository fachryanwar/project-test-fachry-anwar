import React, { useEffect, useState } from "react";

interface HtmlContentProps {
  content: string;
}

const HtmlContent: React.FC<HtmlContentProps> = ({ content }) => {
  const [formattedContent, setFormattedContent] = useState(content);

  useEffect(() => {
    const div = document.createElement("div");
    div.innerHTML = content;
    const paragraphs = div.querySelectorAll("p");
    paragraphs.forEach((paragraph) => {
      const br = document.createElement("br");
      paragraph.parentNode?.insertBefore(br, paragraph.nextSibling);
    });
    setFormattedContent(div.innerHTML);
  }, [content]);

  return (
    <div
      className="prose"
      dangerouslySetInnerHTML={{ __html: formattedContent }}
    />
  );
};

export default HtmlContent;
