import clsxm from "@/lib/clsxm";

enum DividerWeight {
  "sm",
  "kurus",
  "thin",
  "base",
}

const Divider = ({
  weight = "base",
  className,
  orientation = "horizontal",
}: {
  weight?: keyof typeof DividerWeight;
  className?: string;
  orientation?: string;
}) => {
  return (
    <hr
      className={clsxm(
        orientation == "horizontal"
          ? "my-4 w-full border-t-0"
          : "mx-4 h-full border-l-0",
        [
          weight === "sm" &&
            (orientation == "horizontal"
              ? "my-1 h-[2px] bg-white"
              : "mx-1 w-[2px] bg-white"),
          weight === "kurus" &&
            (orientation == "horizontal"
              ? "h-[0.5px] bg-white"
              : "w-[0.5px] bg-white"),
          weight === "thin" &&
            (orientation == "horizontal"
              ? "h-[1px] bg-neutral-200"
              : "w-[1px] bg-neutral-200"),
          weight === "base" &&
            (orientation == "horizontal"
              ? "h-0.5 bg-neutral-300"
              : "w-0.5 bg-neutral-300"),
        ],
        className
      )}
    />
  );
};

export default Divider;
