import Image from "next/image";

enum LogoVariant {
  "white",
  "orange",
}

const Logo = ({
  variant = "white",
}: {
  variant?: keyof typeof LogoVariant;
}) => {
  return (
    <>
      <Image
        src={
          variant === "white" ? "/images/logo.png" : "/images/logo-orange.png"
        }
        alt="logo"
        width={90}
        height={42}
      />
    </>
  );
};

export default Logo;
