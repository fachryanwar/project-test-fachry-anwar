import * as React from "react";
import Typography from "./Typography";

import clsxm from "@/lib/clsxm";

export type SelectInputProps = {
  id: string;
  label?: string;
  labelClassName?: string;
  labelDirection?: string;
  readOnly?: boolean;
} & React.ComponentPropsWithoutRef<"select">;

export default function SelectInput({
  id,
  label,
  labelDirection,
  className,
  readOnly = false,
  defaultValue = "",
  labelClassName,
  children,
  ...rest
}: SelectInputProps) {
  return (
    <div className="w-full">
      <div
        className={clsxm(
          labelDirection === "horizontal" && "gap-2 md:flex md:items-center"
        )}
      >
        {label && (
          <label htmlFor={id} className="flex items-center space-x-1">
            <Typography
              variant="p2"
              weight="medium"
              className={clsxm(labelClassName, "text-slate-700")}
            >
              {label}
            </Typography>
          </label>
        )}

        <select
          id={id}
          name={id}
          defaultValue={defaultValue}
          disabled={readOnly}
          className={clsxm(
            "mt-1 w-full rounded-full py-2.5 text-center lg:mt-0 lg:w-[50%]",
            "ring-1 ring-inset ring-gray-300 focus:ring-inset focus:ring-theme-primary-main",
            readOnly && "cursor-not-allowed",
            className
          )}
          aria-describedby={id}
          {...rest}
        >
          {children}
        </select>
      </div>
    </div>
  );
}
