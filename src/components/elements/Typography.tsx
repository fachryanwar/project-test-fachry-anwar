import clsxm from "@/lib/clsxm";
import { Josefin_Sans, Poppins, Roboto_Condensed } from "next/font/google";

const roboto = Roboto_Condensed({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
});
const josefins = Josefin_Sans({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
});
const poppins = Poppins({
  weight: ["300", "400", "500", "600", "700"],
  subsets: ["latin"],
});

enum TypographyVariant {
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "p1",
  "p2",
  "p3",
  "p4",
}

enum FontVariant {
  "josefins",
  "poppins",
  "roboto",
}

enum FontWeight {
  "light",
  "regular",
  "medium",
  "semibold",
  "bold",
}

type TypographyProps<T extends React.ElementType> = {
  as?: T;
  className?: string;
  weight?: keyof typeof FontWeight;
  font?: keyof typeof FontVariant;
  variant?: keyof typeof TypographyVariant;
  children: React.ReactNode;
};

export default function Typography<T extends React.ElementType>({
  as,
  children,
  className,
  weight = "medium",
  variant = "p1",
  font = "poppins",
  ...props
}: TypographyProps<T> &
  Omit<React.ComponentProps<T>, keyof TypographyProps<T>>) {
  const Component = as || "p";

  return (
    <Component
      className={clsxm(
        // *=============== Font Type ==================
        [font === "josefins" && [josefins.className]],
        [font === "poppins" && [poppins.className]],
        [font === "roboto" && [roboto.className]],
        // *=============== Font Weight ==================
        [weight === "light" && ["font-light"]],
        [weight === "regular" && ["font-normal"]],
        [weight === "medium" && ["font-medium"]],
        [weight === "semibold" && ["font-semibold"]],
        [weight === "bold" && ["font-bold"]],
        // *=============== Font Variants ==================
        [
          variant === "h1" && [
            "text-[40px] md:text-[72px] leading-normal md:leading-tight font-bold",
          ],
          variant === "h2" && [
            "text-[30px] md:text-[60px] leading-normal md:leading-tight font-bold",
          ],
          variant === "h3" && [
            "text-[28px] md:text-[48px] leading-normal md:leading-tight font-bold",
          ],
          variant === "h4" && [
            "text-[24px] md:text-[34px] leading-normal md:leading-tight font-bold",
          ],
          variant === "h5" && [
            "text-[18px] md:text-[24px] leading-normal md:leading-tight font-bold",
          ],
          variant === "h6" && [
            "text-[14px] md:text-[18px] leading-normal md:leading-tight font-bold",
          ],
          variant === "p1" && ["text-[16px] leading-normal md:leading-relaxed"],
          variant === "p2" && ["text-[14px] leading-normal md:leading-relaxed"],
          variant === "p3" && ["text-[12px] leading-normal md:leading-relaxed"],
          variant === "p4" && ["text-[10px] md:leading-[12px]"],
        ],
        className
      )}
      {...props}
    >
      {children}
    </Component>
  );
}
