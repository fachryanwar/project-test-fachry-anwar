import { Dispatch, ReactNode, SetStateAction } from "react";

const ModalLayout = ({
  children,
  isOpen,
  setIsOpen,
}: {
  children: ReactNode;
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
}) => {
  return (
    <div className="fixed left-0 top-0 z-40 flex h-full w-full items-center justify-center bg-black bg-opacity-20">
      <div className="relative h-[80%] w-[80%] overflow-y-auto rounded-xl bg-white px-5 pt-10 md:h-[70%] md:w-[60%] md:px-10">
        {children}
      </div>
    </div>
  );
};

export default ModalLayout;
