import Link from "next/link";
import Button from "../elements/Button";
import Typography from "../elements/Typography";

const DummyPage = ({ link }: { link: string }) => {
  return (
    <div className="flex h-[90vh] items-center justify-center p-5">
      <div className="flex flex-col items-center justify-center rounded-2xl border-2 border-theme-primary-main p-5 md:p-10">
        <Typography variant="h3" className="text-center text-slate-600">
          Sorry...
        </Typography>
        <Typography
          variant="h6"
          className="mt-3 text-center text-gray-600 md:mt-5 font-medium"
        >
          There is no content here. Visit the real page.
        </Typography>
        <Link href={link} target="_blank" className="mt-5">
          <Button>Visit Page</Button>
        </Link>
      </div>
    </div>
  );
};

export default DummyPage;
