import { useRouter } from "next/router";
import React, { useCallback, useEffect, useState } from "react";
import DesktopNavbar from "../fragments/DesktopNavbar";
import Footer from "../fragments/Footer";
import MobileNavigation from "../fragments/MobileNavigation";

type AppShellProps = {
  children: React.ReactNode;
};

const disableNavbar = ["/404"];

const AppShell = (props: AppShellProps) => {
  const { children } = props;
  const { pathname } = useRouter();

  const [show, setShow] = useState(true);
  const [lastScrollY, setLastScrollY] = useState(0);

  const controlNavbar = useCallback(() => {
    if (window.scrollY > lastScrollY) {
      setShow(false);
    } else {
      setShow(true);
    }
    setLastScrollY(window.scrollY);
  }, [lastScrollY]);

  useEffect(() => {
    window.addEventListener("scroll", controlNavbar);
    return () => {
      window.removeEventListener("scroll", controlNavbar);
    };
  }, [controlNavbar]);

  const disabledNavbar = disableNavbar.includes(pathname);

  return (
    <main>
      {!disabledNavbar && (
        <>
          <div className="md:hidden">
            <MobileNavigation show={show} scrollPosition={lastScrollY} />
          </div>
          <div className="hidden md:block">
            {show && <DesktopNavbar show={show} scrollPosition={lastScrollY} />}
          </div>
        </>
      )}
      <div className="min-h-[80vh]">{children}</div>
      {!disabledNavbar && <Footer />}
    </main>
  );
};

export default AppShell;
