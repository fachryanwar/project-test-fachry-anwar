import { socialMedia } from "@/content/socialMedis";
import Link from "next/link";
import Typography from "../elements/Typography";

const Footer = () => {
  return (
    <div className="flex w-full flex-col items-center justify-between gap-4 md:gap-6 bg-white px-5 md:px-12 py-6 md:flex-row border-t-2 border-gray-200">
      <div className="flex flex-col items-center gap-4 md:gap-6 md:flex-row">
        <Typography className="text-center text-gray-500">
          Engage with Us
        </Typography>
        <div className="flex items-center gap-3">
          {socialMedia.map((media) => (
            <Link
              target="_blank"
              key={media.link}
              href={media.link}
              className="text-xl text-theme-primary-main hover:scale-125 hover:duration-200"
            >
              <media.icon />
            </Link>
          ))}
        </div>
      </div>
      <div className="flex flex-col items-center gap-4 md:gap-6 md:flex-row">
        <Link href={"https://suitmedia.com/privacy-policy"} target="_blank">
          <Typography
            className="text-theme-primary-main hover:text-theme-primary-secondary"
            variant="p2"
          >
            Privacy Policy
          </Typography>
        </Link>
        <Typography className="text-center text-gray-500" variant="p2">
          © Suitmedia 2009-2024. All rights reserved
        </Typography>
      </div>
    </div>
  );
};

export default Footer;
