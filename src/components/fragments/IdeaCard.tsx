import { getDateFormat } from "@/lib/formater";
import { IdeaData } from "@/types/ideaResponse";
import Image from "next/image";
import { Dispatch, SetStateAction, useState } from "react";
import Typography from "../elements/Typography";

const replaceUrlDomain = (url: string | null): string => {
  if (url === null || url == undefined) {
    return "/images/default-thumbnail.png";
  }
  return url.replace("assets.suitdev.com", "suitmedia.static-assets.id");
};

const IdeaCards = ({
  idea,
  setModalOpen,
  setSelectedPost,
}: {
  idea: IdeaData;
  setModalOpen: Dispatch<SetStateAction<boolean>>;
  setSelectedPost: Dispatch<SetStateAction<IdeaData | undefined>>;
}) => {
  const [imageSrc, setImageSrc] = useState(
    replaceUrlDomain(idea.medium_image[0]?.url) ||
      replaceUrlDomain(idea.small_image[0]?.url)
  );

  const handleImageError = () => {
    setImageSrc("/images/default-thumbnail.png");
  };

  return (
    <div
      onClick={() => {
        setModalOpen(true);
        setSelectedPost(idea);
      }}
      className="cursor-pointer overflow-hidden rounded-xl bg-white pb-5 shadow-md hover:scale-105 hover:duration-300 md:w-56"
    >
      <div className="relative h-32 w-full md:h-44">
        <Image
          src={imageSrc}
          alt="chef"
          fill
          objectFit="cover"
          objectPosition="center"
          onError={handleImageError}
        />
      </div>
      <div className="mt-3 px-5">
        <Typography
          variant="p2"
          font="roboto"
          className="text-gray-400"
          weight="semibold"
        >
          {getDateFormat(idea.published_at)}
        </Typography>
        <Typography
          className="line-clamp-3 text-slate-700"
          font="roboto"
          variant="h6"
        >
          {idea.title}
        </Typography>
      </div>
    </div>
  );
};

export default IdeaCards;
