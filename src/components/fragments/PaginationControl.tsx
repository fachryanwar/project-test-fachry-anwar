import { GrFormPrevious } from "react-icons/gr";
import {
  MdNavigateNext,
  MdOutlineSkipNext,
  MdOutlineSkipPrevious,
} from "react-icons/md";
import PaginationButton from "../elements/PaginationButton";
import Typography from "../elements/Typography";

const PaginationControl = ({
  pageNumber,
  setPageNumber,
  last_page,
}: {
  pageNumber: number;
  setPageNumber: (page: number) => void;
  last_page: number;
}) => {
  return (
    <div className="mt-5 flex w-full items-center justify-center gap-2">
      <button
        className="rounded-lg p-1 text-xl text-gray-600 hover:bg-gray-200"
        onClick={() => setPageNumber(1)}
      >
        <MdOutlineSkipPrevious />
      </button>
      <button
        className="rounded-lg p-1 text-xl text-gray-600 hover:bg-gray-200 disabled:cursor-not-allowed"
        onClick={() => setPageNumber(pageNumber - 1)}
        disabled={pageNumber == 1}
      >
        <GrFormPrevious />
      </button>
      {3 < pageNumber && (
        <>
          <PaginationButton
            key={1}
            page={1}
            pageNumber={pageNumber}
            setPageNumber={setPageNumber}
          />
          <Typography>...</Typography>
        </>
      )}
      {pageNumber > 1 && (
        <PaginationButton
          key={pageNumber - 1}
          page={pageNumber - 1}
          pageNumber={pageNumber}
          setPageNumber={setPageNumber}
        />
      )}
      <PaginationButton
        key={pageNumber}
        page={pageNumber}
        pageNumber={pageNumber}
        setPageNumber={setPageNumber}
      />
      {pageNumber < last_page && (
        <PaginationButton
          key={pageNumber + 1}
          page={pageNumber + 1}
          pageNumber={pageNumber}
          setPageNumber={setPageNumber}
        />
      )}
      {last_page - 2 > pageNumber && (
        <>
          <Typography>...</Typography>
          <PaginationButton
            key={last_page}
            page={last_page}
            pageNumber={pageNumber}
            setPageNumber={setPageNumber}
          />
        </>
      )}
      <button
        className="rounded-lg p-1 text-xl text-gray-600 hover:bg-gray-200 disabled:cursor-not-allowed"
        disabled={pageNumber == last_page}
        onClick={() => setPageNumber(pageNumber + 1)}
      >
        <MdNavigateNext />
      </button>
      <button
        className="rounded-lg p-1 text-xl text-gray-600 hover:bg-gray-200"
        onClick={() => setPageNumber(last_page)}
      >
        <MdOutlineSkipNext />
      </button>
    </div>
  );
};

export default PaginationControl;
