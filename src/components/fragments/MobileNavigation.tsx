import { menu } from "@/content/navbarMenu";
import clsxm from "@/lib/clsxm";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { GiHamburgerMenu } from "react-icons/gi";
import { IoClose } from "react-icons/io5";
import Logo from "../elements/Logo";
import Typography from "../elements/Typography";

const MobileNavigation = ({
  show,
  scrollPosition,
}: {
  show: boolean;
  scrollPosition: number;
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const { pathname } = useRouter();
  const isActive = (href: string) => pathname == href;

  return (
    <div className={clsxm("relative")}>
      <div
        className={clsxm(
          "fixed left-0 top-0 z-20 flex w-full items-center justify-between bg-theme-primary-main px-4 py-2",
          show ? "fade-in-down" : "fade-out-up",
          scrollPosition > 0 ? "bg-opacity-80" : "bg-opacity-100"
        )}
      >
        <Link href={"/"}>
          <Logo />
        </Link>
        <button
          className="text-3xl text-white hover:text-gray-200"
          onClick={() => setIsOpen(true)}
        >
          <GiHamburgerMenu />
        </button>
      </div>

      {isOpen && (
        <div
          className={clsxm(
            "fixed left-0 top-0 z-30 min-h-screen w-full bg-white py-5",
            isOpen ? "fade-in-right" : "fade-out-left"
          )}
        >
          <div className="flex items-center justify-between px-5">
            <Link href={"/"} onClick={() => setIsOpen(false)}>
              <Logo variant="orange" />
            </Link>
            <button
              className="text-4xl text-slate-700 hover:text-theme-primary-main"
              onClick={() => setIsOpen(false)}
            >
              <IoClose />
            </button>
          </div>
          <ul className="mt-10 flex flex-col gap-5 px-8">
            {menu.map((menuItem) => (
              <Link
                href={menuItem.href}
                key={menuItem.name}
                onClick={() => setIsOpen(false)}
              >
                <Typography
                  variant="h4"
                  className={clsxm(
                    isActive(menuItem.href)
                      ? "text-theme-primary-main"
                      : "text-slate-700 hover:text-theme-primary-secondary"
                  )}
                >
                  {menuItem.name}
                </Typography>
              </Link>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default MobileNavigation;
