import { getDateFormat } from "@/lib/formater";
import { IdeaData } from "@/types/ideaResponse";
import { Dispatch, SetStateAction, useEffect, useRef } from "react";
import { IoCloseCircleOutline } from "react-icons/io5";
import Divider from "../elements/Divider";
import HtmlContent from "../elements/HtmlContent";
import Typography from "../elements/Typography";

const DetailPost = ({
  post,
  setIsOpen,
}: {
  post: IdeaData;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
}) => {
  const modalRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (
        modalRef.current &&
        !modalRef.current.contains(event.target as Node)
      ) {
        setIsOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [modalRef, setIsOpen]);

  return (
    <div ref={modalRef}>
      <button
        className="absolute right-5 top-5 text-3xl text-gray-500 hover:text-slate-700"
        onClick={() => setIsOpen(false)}
      >
        <IoCloseCircleOutline />
      </button>
      <Typography
        variant="h4"
        font="poppins"
        className="w-[98%] text-theme-primary-main"
      >
        {post.title}
      </Typography>
      <Typography className="text-gray-400" font="roboto">
        {getDateFormat(post.published_at)}
      </Typography>
      <Divider weight="base" />
      <Typography className="text-justify text-slate-700" variant="p2">
        <HtmlContent content={post.content} />
      </Typography>
    </div>
  );
};

export default DetailPost;
