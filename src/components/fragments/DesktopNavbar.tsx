import { menu } from "@/content/navbarMenu";
import clsxm from "@/lib/clsxm";
import Link from "next/link";
import { useRouter } from "next/router";
import Divider from "../elements/Divider";
import Logo from "../elements/Logo";
import Typography from "../elements/Typography";

const DesktopNavbar = ({
  show,
  scrollPosition,
}: {
  show: boolean;
  scrollPosition: number;
}) => {
  const { pathname } = useRouter();

  return (
    <nav
      className={clsxm(
        "fixed left-0 top-0 z-20 flex w-full items-center justify-between bg-theme-primary-main px-20 py-3",
        show ? "fade-in-down" : "fade-out-up",
        scrollPosition > 0 ? "bg-opacity-80" : "bg-opacity-100"
      )}
    >
      <Link href={"/"}>
        <Logo />
      </Link>
      <ul className="flex items-start gap-10">
        {menu.map((menuItem) => (
          <Link
            key={menuItem.name}
            href={menuItem.href}
            className="group text-white"
          >
            <Typography className="group-hover:text-slate-700" variant="p2">
              {menuItem.name}
            </Typography>
            {pathname == menuItem.href && (
              <Divider weight="sm" className="group-hover:bg-slate-700" />
            )}
          </Link>
        ))}
      </ul>
    </nav>
  );
};

export default DesktopNavbar;
