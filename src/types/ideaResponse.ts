export interface IdeaResponse {
  data: IdeaData[];
  links: Links;
  meta: IdeaMetaData;
}

export interface IdeaData {
  content: string;
  created_at: string;
  deleted_at: string;
  id: number;
  published_at: string;
  slug: string;
  title: string;
  updated_at: string;
  medium_image: IdeaImage[];
  small_image: IdeaImage[];
}

export interface IdeaImage {
  file_name: string;
  url: string;
}

export interface Links {
  first: string;
  last: string;
  next: string;
  prev: string;
}

export interface IdeaMetaData {
  current_page: number;
  from: number;
  to: number;
  last_page: number;
  per_page: number;
  total: number;
}
