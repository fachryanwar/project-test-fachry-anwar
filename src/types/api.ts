export interface SuccessResponse<T> {
  result: T;
  message: string;
}

export interface ErrorResponse {
  error: string;
}
