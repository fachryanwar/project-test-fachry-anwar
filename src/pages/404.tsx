import Button from "@/components/elements/Button";
import Typography from "@/components/elements/Typography";
import Head from "next/head";
import Link from "next/link";

const Error404 = () => {
  return (
    <main className="flex min-h-screen w-full flex-col items-center justify-center">
      <Head>
        <title>404 | Not Found</title>
      </Head>
      <Typography variant="h1" className="text-slate-700">
        404
      </Typography>
      <Typography variant="h6" className="text-slate-700">
        Page not found.
      </Typography>
      <Link href={"/"}>
        <Button className="mt-5">Go to Home</Button>
      </Link>
    </main>
  );
};

export default Error404;
