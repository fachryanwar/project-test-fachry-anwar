import Button from "@/components/elements/Button";
import Typography from "@/components/elements/Typography";
import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

const AboutPage = () => {
  return (
    <section className="flex min-h-[90vh] items-center justify-center p-5">
      <Head>
        <title>Suitmedia | About</title>
      </Head>
      <div className="mt-20 flex w-[80%] flex-col items-center justify-center gap-5 md:gap-10 rounded-2xl border-2 border-theme-primary-main p-5 md:w-[60%] md:flex-row">
        <Image
          src={"/images/photo.png"}
          alt="photo"
          width={300}
          height={300}
          className="rounded-xl bg-gradient-to-r from-red-200 via-red-300 to-yellow-200 px-5 pt-5"
        />
        <div className="flex flex-col gap-3">
          <Typography variant="h3" className="text-slate-700">
            Hello! 👋🏼
          </Typography>
          <Typography className="text-gray-600">
            Hi there! I&apos;m Fachry Anwar, a passionate frontend enthusiast.
            As I step into the final year of my studies, embarking on the
            seventh semester, I&apos;m thrilled to share this journey with you.
            Let&apos;s create something amazing together!
          </Typography>
          <Link href={"https://fachryanwar.vercel.app/"} className="mt-5">
            <Button>Visit Me</Button>
          </Link>
        </div>
      </div>
    </section>
  );
};

export default AboutPage;
