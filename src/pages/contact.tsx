import DummyPage from "@/components/layout/DummyPage";
import Head from "next/head";

const ContactPage = () => {
  return (
    <>
      <Head>
        <title>Suitmedia | Contact</title>
      </Head>
      <DummyPage link="https://suitmedia.com/contact" />
    </>
  );
};

export default ContactPage;
