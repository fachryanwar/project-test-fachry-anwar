import SelectInput from "@/components/elements/SelectInput";
import Typography from "@/components/elements/Typography";
import DetailPost from "@/components/fragments/DetailPost";
import IdeaCards from "@/components/fragments/IdeaCard";
import PaginationControl from "@/components/fragments/PaginationControl";
import ModalLayout from "@/components/layout/ModalLayout";
import { getFromLocalStorage, saveToLocalStorage } from "@/lib/localStorage";
import sendRequest from "@/lib/requestHandler";
import { IdeaData, IdeaResponse } from "@/types/ideaResponse";
import Head from "next/head";
import { useEffect, useState } from "react";

const IdeasPage = () => {
  const [isModalOpen, setModalOpen] = useState(false);
  const [selectedPost, setSelectedPost] = useState<IdeaData>();

  const [ideaResponse, setIdeaResponse] = useState<IdeaResponse>();
  const [sort, setSort] = useState<string>(
    getFromLocalStorage("sortMethod", "-published_at") as string
  );
  const [pageSize, setPageSize] = useState<number>(
    getFromLocalStorage("pageSize", 10) as number
  );
  const [pageNumber, setPageNumber] = useState<number>(
    getFromLocalStorage("pageNumber", 1) as number
  );

  const handleChangePageSize = (
    event: React.ChangeEvent<HTMLSelectElement>
  ) => {
    setPageSize(parseInt(event.target.value));
    handleChangePageNumber(1);
    saveToLocalStorage("pageSize", event.target.value);
  };
  const handleChangeSort = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSort(event.target.value);
    saveToLocalStorage("sortMethod", event.target.value);
  };
  const handleChangePageNumber = (page: number) => {
    setPageNumber(page);
    saveToLocalStorage("pageNumber", page);
  };

  useEffect(() => {
    const fetchData = async () => {
      const [result, isSuccess] = await sendRequest(
        "get",
        `/ideas?page[number]=${pageNumber}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sort}`
      );

      if (isSuccess) {
        setIdeaResponse(result as IdeaResponse);
      }
    };

    fetchData();
  }, [pageSize, sort, pageNumber]);

  return (
    <main className="min-h-screen">
      <Head>
        <title>Suitmedia | Ideas</title>
      </Head>
      <div className="shape relative flex w-full flex-col items-center justify-center bg-idea-background bg-cover bg-fixed bg-center">
        <div className="absolute -z-10 h-full w-full bg-black bg-opacity-20"></div>
        <Typography className="font-medium text-white" variant="h2">
          Ideas
        </Typography>
        <Typography className="font-medium text-white" variant="h6">
          Where all our great things begin
        </Typography>
      </div>
      <div className="mt-10 flex flex-col justify-between gap-4 px-6 md:px-14 lg:flex-row-reverse lg:items-center lg:px-20">
        <div className="flex w-full items-center gap-3 md:justify-end lg:w-[50%]">
          {pageSize && (
            <SelectInput
              id="pageSize"
              label="Show per page"
              labelDirection="horizontal"
              value={pageSize}
              onChange={handleChangePageSize}
            >
              <option value={10}>10</option>
              <option value={20}>20</option>
              <option value={50}>50</option>
            </SelectInput>
          )}
          {sort && (
            <SelectInput
              id="sort"
              label="Sort by"
              labelDirection="horizontal"
              onChange={handleChangeSort}
              value={sort}
            >
              <option value="-published_at">Newest</option>
              <option value="published_at">Oldest</option>
            </SelectInput>
          )}
        </div>
        {ideaResponse && (
          <Typography variant="p2" className="text-slate-700">
            Showing {ideaResponse.meta.from} - {ideaResponse.meta.to} of{" "}
            {ideaResponse.meta.total}
          </Typography>
        )}
      </div>
      <div className="mt-4 grid w-full grid-cols-2 place-content-center content-center justify-items-center gap-6 px-6 md:grid-cols-3 md:px-14 lg:grid-cols-4 lg:px-20">
        {ideaResponse &&
          ideaResponse.data.map((ideaItem) => (
            <IdeaCards
              idea={ideaItem}
              key={ideaItem.id}
              setModalOpen={setModalOpen}
              setSelectedPost={setSelectedPost}
            />
          ))}
      </div>
      {ideaResponse && (
        <PaginationControl
          pageNumber={pageNumber}
          setPageNumber={handleChangePageNumber}
          last_page={ideaResponse.meta.last_page}
        />
      )}
      {isModalOpen && ideaResponse && selectedPost && (
        <ModalLayout isOpen={isModalOpen} setIsOpen={setModalOpen}>
          <DetailPost post={selectedPost} setIsOpen={setModalOpen} />
        </ModalLayout>
      )}
    </main>
  );
};

export default IdeasPage;
