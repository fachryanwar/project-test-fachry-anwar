import DummyPage from "@/components/layout/DummyPage";
import Head from "next/head";

const CareerPage = () => {
  return (
    <>
      <Head>
        <title>Suitmedia | Career</title>
      </Head>
      <DummyPage link="https://suitmedia.com/careers" />
    </>
  );
};

export default CareerPage;
