import Button from "@/components/elements/Button";
import Divider from "@/components/elements/Divider";
import Typography from "@/components/elements/Typography";
import Head from "next/head";
import Link from "next/link";
import { BsChatRightDots, BsGear } from "react-icons/bs";
import { IoExtensionPuzzleOutline } from "react-icons/io5";
import { SiMinds } from "react-icons/si";

export default function Home() {
  return (
    <main>
      <Head>
        <title>Suitmedia</title>
      </Head>
      <section className="relative h-[100vh] w-full bg-home-background bg-cover bg-fixed bg-top">
        <div className="absolute z-10 flex h-[100%] w-full flex-col justify-center bg-black bg-opacity-50 px-10 md:px-16">
          <Typography variant="h6" className="font-light text-white">
            Step into our Innovative Space, where Digital Excellence Meets
            Exceptional Creativity
          </Typography>
          <Typography
            variant="h3"
            className="mt-4 font-semibold text-white md:w-[60%]"
          >
            Navigate the Innovation to Unlock Digital Challenges
          </Typography>
          <Link href={"https://suitmedia.com/about"} className="mt-10">
            <Button rounded="lg" variant="outline">
              Discover More
            </Button>
          </Link>
          <div className="mt-14 hidden flex-wrap items-center md:gap-5 lg:flex">
            <div>
              <Typography className="text-white" variant="h4">
                15+
              </Typography>
              <Typography className="text-gray-300">
                Years of Excellence
              </Typography>
            </div>
            <Divider orientation="vertical" />
            <div>
              <Typography className="text-white" variant="h4">
                900+
              </Typography>
              <Typography className="text-gray-300">
                Digital Projects
              </Typography>
            </div>
            <Divider orientation="vertical" />
            <div>
              <Typography className="text-white" variant="h4">
                150+
              </Typography>
              <Typography className="text-gray-300">Happy Clients</Typography>
            </div>
            <Divider orientation="vertical" />
            <div>
              <Typography className="text-white" variant="h4">
                200+
              </Typography>
              <Typography className="text-gray-300">
                In-House Experts
              </Typography>
            </div>
            <Divider orientation="vertical" />
          </div>
        </div>
      </section>
      <section className="flex flex-col items-center px-5 py-5 md:p-10">
        <Typography
          variant="h4"
          className="mx-auto text-center font-semibold text-slate-700 md:w-[50%]"
        >
          We Guide Your Brand Transformation to Excellence
        </Typography>
        <div className="mt-10 flex flex-col gap-4 md:grid md:grid-cols-2 md:gap-y-10 md:px-10">
          <div className="flex items-start gap-2">
            <IoExtensionPuzzleOutline className="w-[40%] text-4xl text-gray-500 md:w-[20%] md:text-3xl" />
            <div>
              <Typography
                variant="h6"
                className="font-medium leading-none text-gray-500 md:leading-none"
              >
                Strategy
              </Typography>
              <Typography variant="p2" className="mt-3 text-gray-500">
                We assist you in evolving digital transformation through
                practical strategy by providing Digital & IT Advisory and
                Development Consultation, and developing problem-solving plans
                by doing UX & Market Research and Monitoring & Analytics.
              </Typography>
            </div>
          </div>
          <div className="flex items-start gap-2">
            <SiMinds className="w-[40%] text-4xl text-gray-500 md:w-[20%] md:text-3xl" />
            <div>
              <Typography
                variant="h6"
                className="font-medium leading-none text-gray-500 md:leading-none"
              >
                Creative
              </Typography>
              <Typography variant="p2" className="mt-3 text-gray-500">
                We&apos;re creative storytellers fueling brand growth with Brand
                Development, UI/UX Design for user-friendly apps, compelling
                Content Creation, and impactful Motion & Videography exclusively
                tailored to your brand.
              </Typography>
            </div>
          </div>
          <div className="flex items-start gap-2">
            <BsGear className="w-[40%] text-4xl text-gray-500 md:w-[20%] md:text-3xl" />
            <div>
              <Typography
                variant="h6"
                className="font-medium leading-none text-gray-500 md:leading-none"
              >
                Technology
              </Typography>
              <Typography variant="p2" className="mt-3 text-gray-500">
                We excel in iOS and Android Development, crafting top-notch
                mobile apps. Our Web Development ensures seamless functionality.
                Boost your business with E-commerce Solutions, and enhance
                visibility with Search Engine Optimization for first-page
                prominence.
              </Typography>
            </div>
          </div>
          <div className="flex items-start gap-2">
            <BsChatRightDots className="w-[40%] text-4xl text-gray-500 md:w-[20%] md:text-2xl" />
            <div>
              <Typography
                variant="h6"
                className="font-medium leading-none text-gray-500 md:leading-none"
              >
                Communication
              </Typography>
              <Typography variant="p2" className="mt-3 text-gray-500">
                We craft 360° digital campaigns, from Creative Campaigns to
                Social Media Management, boosting sales. Drive awareness and
                engagement through Digital Advertising and connect with
                influencers using Influencer Marketing.
              </Typography>
            </div>
          </div>
        </div>
        <Link href={"https://suitmedia.com/expertises"} className="mt-10">
          <Button rounded="lg">Explore Our Expertise</Button>
        </Link>
      </section>
    </main>
  );
}
