import DummyPage from "@/components/layout/DummyPage";
import Head from "next/head";

const WorkPage = () => {
  return (
    <>
      <Head>
        <title>Suitmedia | Work</title>
      </Head>
      <DummyPage link="https://suitmedia.com" />
    </>
  );
};

export default WorkPage;
