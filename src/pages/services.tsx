import DummyPage from "@/components/layout/DummyPage";
import Head from "next/head";

const ServicesPage = () => {
  return (
    <>
      <Head>
        <title>Suitmedia | Services</title>
      </Head>
      <DummyPage link="https://suitmedia.com/expertises" />
    </>
  );
};

export default ServicesPage;
