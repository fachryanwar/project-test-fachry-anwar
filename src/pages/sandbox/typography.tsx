import Typography from "@/components/elements/Typography";
import Head from "next/head";

const TypographyPages = () => {
  return (
    <div className="p-5 md:p-10">
      <Head>
        <title>Typography</title>
      </Head>
      <Typography
        variant="h5"
        font="josefins"
        weight="bold"
        className="mb-4 text-white"
      >
        Typography
      </Typography>
      <div className="my-2 hidden w-full gap-5 rounded-full bg-slate-200 px-5 py-2 text-center text-slate-900 md:flex">
        <div className="w-[12%]">Primary</div>
      </div>
      <div className="grid w-full grid-cols-3 gap-5 rounded-xl border-2 border-primary-20 bg-white p-5 md:flex md:flex-row">
        <div className="md:w-[12%]">
          <div className="w-full rounded-xl border border-gray-200 bg-gray-100 p-2 text-center md:hidden">
            <Typography variant="p1" weight="medium">
              Primary
            </Typography>
          </div>
          <Typography variant="h1" font="josefins" weight="bold">
            H1
          </Typography>
          <Typography variant="h2" font="josefins" weight="bold">
            H2
          </Typography>
          <Typography variant="h3" font="josefins" weight="bold">
            H3
          </Typography>
          <Typography variant="h4" font="josefins" weight="bold">
            H4
          </Typography>
          <Typography variant="h5" font="josefins" weight="bold">
            H5
          </Typography>
          <Typography variant="h6" font="josefins" weight="bold">
            H6
          </Typography>
        </div>
      </div>
    </div>
  );
};

export default TypographyPages;
