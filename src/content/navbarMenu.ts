export type NavbarMenu = {
  name: string;
  href: string;
};

export const menu: NavbarMenu[] = [
  {
    name: "Work",
    href: "/work",
  },
  {
    name: "About",
    href: "/about",
  },
  {
    name: "Services",
    href: "/services",
  },
  {
    name: "Ideas",
    href: "/ideas",
  },
  {
    name: "Careers",
    href: "/careers",
  },
  {
    name: "Contact",
    href: "/contact",
  },
];
