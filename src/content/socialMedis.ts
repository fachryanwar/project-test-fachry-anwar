import { IconType } from "react-icons";
import { AiFillInstagram, AiFillLinkedin, AiFillYoutube } from "react-icons/ai";
import { FaFacebookF, FaSpotify, FaTwitter } from "react-icons/fa";
import { TbCircleLetterF } from "react-icons/tb";

export interface SocialMedia {
  name: string;
  icon: IconType;
  link: string;
}

export const socialMedia: SocialMedia[] = [
  {
    name: "Instagram",
    icon: AiFillInstagram,
    link: "https://www.instagram.com/suitmedia/",
  },
  {
    name: "Linkedin",
    icon: AiFillLinkedin,
    link: "https://www.linkedin.com/company/suitmedia/",
  },
  {
    name: "Youtube",
    icon: AiFillYoutube,
    link: "https://www.youtube.com/@suitmedia",
  },
  {
    name: "Facebook",
    icon: FaFacebookF,
    link: "https://www.facebook.com/suitmedia",
  },
  {
    name: "Twitter",
    icon: FaTwitter,
    link: "https://x.com/suitmedia",
  },
  {
    name: "Spotify",
    icon: FaSpotify,
    link: "https://open.spotify.com/show/7D2MQnYYeui5DsXzfWMLXF?si=BqHBhaqsQum_5pHm-fzYfQ&nd=1",
  },
  {
    name: "My Portofolio",
    icon: TbCircleLetterF,
    link: "https://fachryanwar.vercel.app/",
  },
];
