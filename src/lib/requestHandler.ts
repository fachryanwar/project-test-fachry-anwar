import {
  DANGER_TOAST,
  dismissLoadingToast,
  showLoadingToast,
  showToast,
} from "@/components/elements/Toast";
import api from "@/lib/api";
import { IdeaResponse } from "@/types/ideaResponse";
import { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";

const sendRequest = async <T>(
  httpMethod: string,
  url: string,
  data?: any,
  toast?: boolean
): Promise<[IdeaResponse | string, boolean]> => {
  try {
    showLoadingToast();
    const addAuthorizationHeader = (
      config: AxiosRequestConfig
    ): AxiosRequestConfig => {
      return {
        ...config,
      };
    };

    const apiResponse: AxiosResponse<IdeaResponse> = await api(
      addAuthorizationHeader({ method: httpMethod, url, data })
    );
    
    dismissLoadingToast();

    return [apiResponse.data as IdeaResponse, true];
  } catch (error) {
    dismissLoadingToast();
    if (error instanceof AxiosError) {
      showToast(error.message, DANGER_TOAST);
      return [error.message, false];
    } else {
      showToast("Terjadi kesalahan", DANGER_TOAST);
      return ["Terjadi kesalahan", false];
    }
  }
};

export default sendRequest;
