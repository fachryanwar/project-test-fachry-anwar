const monthNames: string[] = [
  "JANUARI",
  "FEBRUARI",
  "MARET",
  "APRIL",
  "MEI",
  "JUNI",
  "JULI",
  "AGUSTUS",
  "SEPTEMBER",
  "OKTOBER",
  "NOVEMBER",
  "DESEMBER",
];

export const getDateFormat = (inputDate: string): string => {
  const [datePart, timePart] = inputDate.split(" ");

  const [year, month, day] = datePart.split("-");

  const monthName: string = monthNames[parseInt(month) - 1];
  return `${day} ${monthName} ${year}`;
};
