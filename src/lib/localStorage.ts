export function saveToLocalStorage(key: string, value: any): void {
  if (typeof window !== "undefined" && window.localStorage) {
    if (typeof value === "object") {
      localStorage.setItem(key, JSON.stringify(value));
    } else {
      localStorage.setItem(key, value);
    }
  }
}

export function getFromLocalStorage<T>(key: string, defaultValue: T): T {
  if (typeof window !== "undefined" && window.localStorage) {
    const storedValue = localStorage.getItem(key);
    if (storedValue) {
      try {
        return JSON.parse(storedValue) as T;
      } catch (error) {
        return storedValue as unknown as T;
      }
    }
  }
  return defaultValue;
}
